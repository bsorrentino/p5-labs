import p5 from "p5"
import { BACKGROUND } from './common'
import { timeLine as timeLineFactory, TimeLine} from './timeline'

export const fps = 30


class Watch {
  ticks = 0;

  tick( onSecond: () => void) {
    if( ++this.ticks%fps == 0 ) {
      onSecond( ) 
      
    }
  }


}

function s(k$: p5) {
    
  let a = 0;
  let seconds = 0
  
  let timeLine = new Array<TimeLine>()

  let watch = new Watch()

  k$.setup = () => { 
    k$.createCanvas(600,400);
    k$.frameRate(fps);


    timeLine.push( timeLineFactory( k$, { label:'operator 1', y:60} ) );
    timeLine.push( timeLineFactory( k$, { label:'operator 2', y:100} ) );
    timeLine.push( timeLineFactory( k$, { label:'operator 3', y:140} ) );
  }

  k$.draw = () => {
    
    k$.background(BACKGROUND);
    
    // Typography
    k$.textSize(30);
    k$.textAlign(k$.CENTER, k$.CENTER);
    k$.text( seconds, 30, 20 )

    watch.tick( () => {
      ++seconds 

      if( seconds <  60) {

        const tlindex = k$.floor(k$.random( 0, 3))
        //console.log( tlindex )
        timeLine[tlindex].addItem( String(seconds) )
        
      }

    })

    timeLine.forEach( tl => tl.draw( k$ ))
  };


}

const _ = new p5(s);