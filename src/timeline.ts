import p5 from "p5"

import { Boundary, BACKGROUND} from './common'
import { Item } from './item'

type Props = {
  label:string;
  y:number;
}

export function timeLine( k$:p5, props:Props ) {
  return new TimeLine( { left:100, right:k$.width }, props)
}

export class TimeLine  {
    private _lastItem:Item|undefined
    private _items = Array<Item>();
  
    constructor( private boundary:Boundary, private props:Props ) {
      //console.log( this.boundary )
    }

    addItem( label:string ) {

      const x = this.boundary.left + Item.D + (this._items.length * (Item.D + 12 ))
  
      const item = new Item( label, x, this.props.y )
  
      this._items.push( item );
  
      this._lastItem = item

      return item;
    }

    private drawItems( k$: p5 ) { 

      const scroll =  this._lastItem?.isPartialVisibleR( this.boundary ) || 
                      this._lastItem?.isNotVisibleR( this.boundary )  ? 1 : 0
  
      this._items.filter( item => !item.isNotVisibleL(this.boundary) ).forEach( (item,i) =>  {
    
        item.scrollOffsetX += scroll
        item.draw(k$)
  
      })

    }

    draw( k$: p5 ) {
      // Items
      this.drawItems( k$ );

      // Line

      k$.stroke(255)
      k$.line( this.boundary.left, this.props.y, this.boundary.right, this.props.y)
      // Rect
      k$.noStroke()
      k$.fill( BACKGROUND )
      k$.rect( 0, this.props.y - Item.D/2, this.boundary.left, Item.D )

      // Typography

      k$.fill( 255 )
      k$.textSize(18)
      k$.textAlign(k$.LEFT, k$.CENTER);

      k$.text( this.props.label, 0, this.props.y )

    } 
  
  } 
  